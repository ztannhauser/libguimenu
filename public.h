struct entry
{
	char* text;
	int strlen;
	int width;
};

struct menu
{
	struct array entries;
};

struct menu* new_menu(struct guirouter* router, ...);

struct submenu* new_submenu(char* text, struct memu* submenu);

struct entry_with_value* new_entry_with_value(char* text, void* value);
